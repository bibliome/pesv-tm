"
  
    
    
    
    
      
  Characterization of Pyrenophora Species Causing Brown Leaf Spot on Italian Ryegrass ( Lolium multiflorum) in Southwestern China


    
  
"
"
              
                


  
    
      
      Drechslera leaf spot (DLS) caused by Pyrenophora (Drechslera) species is one of the most serious diseases affecting Italian ryegrass (Lolium multiflorum) in China. Between 2015 and 2018, this disease was observed in three Italian ryegrass fields in the province of Sichuan, China. Average leaf disease incidence was approximately 1 to 12% but could range up to 100%. Symptoms appeared as brown or tan spots surrounded by a yellow halo, or brown to dark brown net blotch; subsequently, spots increased in number and size, and they later covered a large area of leaf, eventually causing leaf death. In this study, 86 strains of Pyrenophora fungi were isolated from leaf lesions of Italian ryegrass. Coupled with phylogenetic analysis of the internal transcribed spacer region, partial 28S ribosomal RNA gene, and glyceraldehyde-3-phosphate dehydrogenase gene, morphological characteristics showed that Pyrenophora dictyoides and P. nobleae are associated with Italian ryegrass in southwest China. Pathogenicity tests confirmed that both species can infect Italian ryegrass, causing leaf spot, whereas the virulence of the two species differed; P. nobleae showed lower pathogenicity to Italian ryegrass. This is the first time that these two Pyrenophora species were formally reported on Italian ryegrass based on both morphological and molecular characters. Overall, this study improves knowledge of the Pyrenophora species associated with Italian ryegrass and provides a foundation for control of this disease in the future.
    
  

  


              
            "
