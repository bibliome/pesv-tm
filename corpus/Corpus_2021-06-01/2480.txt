"
  
    
    
    
    
      
  Cytochrome P450 metabolism mediates low-temperature resistance in pinewood nematode


    
  
"
"
              
                


  
    
      
      Pinewood nematode (PWN; Bursaphelenchus xylophilus) is a devastating invasive species that is expanding into colder regions. Here, we investigated the molecular mechanisms underlying low-temperature resistance of PWN. We identified differentially expressed genes enriched under low temperature in previously published transcriptome data using the Kyoto Encyclopedia of Genes and Genomes. Quantitative real-time PCR was used to further validate the transcript level changes of three known cytochrome P450 genes under low temperature. RNA interference was used to validate the low-temperature resistance function of three cytochrome P450 genes from PWN. We report that differentially expressed genes were significantly enriched in two cytochrome P450-related pathways under low-temperature treatment. Heatmap visualization of transcript levels of cytochrome P450-related genes revealed widely different transcript patterns between PWNs treated under low and regular temperatures. Transcript levels of three cytochrome P450 genes from PWNs were elevated at low temperature, and knockdown of these genes decreased the survival rates of PWNs under low temperature. In summary, these findings suggest that cytochrome P450 metabolism plays a critical role in the low-temperature resistance mechanism of PWN.
    
  

  


              
            "
