"
  
    
    
    
    
      
  First Report of Arthrinium kogelbergense Causing Blight Disease of Bambusa intermedi a in Sichuan Province, China


    
  
"
"
              
                


  
    
      
      Bambusa intermedia Hsueh et Yi, a species in the Gramineae family, is mainly distributed in southern China and is commonly found in the Sichuan, Yunnan, and Guangdong Provinces and in the Guangxi Autonomous Region. It is economically significant as a building material, a food source, and for applications in various other raw products. In July 2017, a blight disease was found on B. intermedia stems, affecting approximately 40% of 9600 plants in the Changning and Jiangan counties of Sichuan Province, China. In the early stages of the disease, leaves at the top of the plants withered and yellowed, eventually falling off. Subsequently, the stems and upper branches discolored and were dead to varying degrees. To isolate the causal fungus, 100 samples from B. intermedia culms and branches were collected from symptomatic plants in Changning and Jiangan counties. Small sections (4 to 5 mm2) were surface-sterilized for 30 s in 3% sodium hypochlorite and 60 s in 75% ethanol. The samples were then rinsed three times in sterile water, placed onto potato dextrose agar (PDA) amended with streptomycin sulfate (50 µg/ml, Solarbio, Beijing), and incubated in a dark incubator at 25 ± 1°C for 6 days. A total of 120 isolates were obtained, of which 89 isolates had similar colony morphology and conidial measurements. Colony characteristics of the fungus on the PDA surface were dirty white, reverse dirty white to pale luteous, colonies flat, floccose, with moderate aerial mycelium. Hyphae were smooth, hyaline, branched, and septate. Conidiophores were erect, septate, pale brown, smooth, and reduced to conidiogenous cells. Conidiogenous cells were pale brown, smooth, and ampulliform. Conidia were 5.69-8.03 × 4.32-7.30 μm (30 conidia per isolate, Leica Application Suite X 3.4.1.17822) single celled, brown to black, smooth, and globose to ellipsoid shape, mostly globose in surface view and lenticular in side view with an equatorial ring. Based on morphological characteristics, it was identified as an Arthrinium species (Crous and Groenwald 2013). PCRs were performed with primers ITS4/ITS5 for the ITS region (White et al. 1990), primers T1/Bt2b for the β-tubulin gene (TUB) (O'Donnell and Cigelnik 1997, Glass and Donaldson 1995) , primers 5.8sR/LR5 (Vilgalys and Hester 1990) and LR0R/TW13 (Hamayun et al. 2009) for 28S large-subunit (LSU) rDNA. Newly generated representative sequences were deposited in GenBank, ITS sequence (GenBank No. MT415395), TUB sequence (MT415365), and LSU sequence (MT415396, MT415398). Comparison to ex-type isolate sequences showed strong homology with Arthrinium kogelbergense Crous (CBS 113333, Crous and Groenwald 2013). ITS: 99.65% identity to Accession NR_120272; TUB: 99.75% identity to Accession KF144984; LSU: 99.60% and 100.00% identity to Accession NG_042779. To confirm its pathogenicity and to fulfill Koch's postulates, 20 one-year-old B. intermedia plants were wounded with a sterile needle at a depth of 1 mm in the stems and twigs, and were inoculated with a 5-mm-diameter disk of PDA that was colonized by the isolates (2 plants per isolate in each inoculation experiment, 10 repetitions) (Li et al. 2016). Ten control plants were treated similarly except that they were mock inoculated with PDA plugs without the fungus. All plants were kept at 25-28°C and covered with plastic bags to maintain high relative humidity (90-95%) on a 12-h light/dark incubation. Thirty days later, the inoculated plants showed the same symptoms observed originally, and the controls remained healthy. The same fungus was reisolated from the infected stems and twigs and showed similar morphological characteristics and molecular traits. To our knowledge, this is the first report of A. kogelbergense as a causal agent of blight disease on B. intermedia in Sichuan Province, China.
    
  

  


              
            "
