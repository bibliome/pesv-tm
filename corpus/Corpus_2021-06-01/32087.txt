"
  
    
    
    
    
      
  Establishment of a Cuscuta campestris-mediated enrichment system for genomic and transcriptomic analyses of 'Candidatus Liberibacter asiaticus'


    
  
"
"
              
                


  
    
      
      'Candidatus Liberibacter asiaticus' (CLas) is a phloem-limited non-culturable α-proteobacterium associated with citrus Huanglongbing, a highly destructive disease threatening global citrus industry. Research on CLas is challenging due to the current inability to culture CLas in vitro and the low CLas titre in citrus plant. Here, we develop a CLas enrichment system using the holoparasitic dodder plant (Cuscuta campestris) as an amenable host to acquire and enrich CLas from CLas-infected citrus shoots maintained hydroponically. Forty-eight out of fifty-five (87%) dodder plants successfully parasitized CLas-infected citrus shoots with detectable CLas by PCR. Among 48 dodders cultures, 30 showed two- to 419-fold CLas titre increase as compared to the corresponding citrus hosts. The CLas population rapidly increased and reached the highest level in dodder tendrils at 15 days after parasitizing citrus shoot. Genome sequencing and assembly derived from CLas-enriched dodder DNA samples generated a higher resolution than those obtained for CLas from citrus hosts. No genomic variation was detected in CLas after transmission from citrus to dodder during short-term parasitism. Dual RNA-Seq experiments showed similar CLas gene expression profiles in dodder and citrus samples, yet dodder samples generated a higher resolution of CLas transcriptome data. The ability of dodder to support CLas multiplication to high levels, as well as its advantage in CLas genomic and transcriptomic analyses, make it an optimal model for further studies on CLas-host interaction.
    
  

  


              
            "
