"
  
    
    
    
    
      
  First Report of Fusarium Wilt Caused by Fusarium equiseti on Cabbage (Brassica oleracea var. capitate L.) in Korea


    
  
"
"
              
                


  
    
      
      Cabbage (Brassica oleracea var. capitate L.) is an important vegetable crop that is widely cultivated throughout the world. In August 2019, wilting symptoms on cabbage (stunted growth, withered leaves, and wilted plants) were observed in a cabbage field of Pyeongchang, Gangwon Province, with an incidence of 5 to 10%. To identify the cause, symptomatic root tissue was excised, surface-sterilized with 70% ethanol, and rinsed thrice with sterile distilled water. The samples were dried on blotter paper, placed onto potato dextrose agar (PDA), and incubated at 25°C for 1 week. Five morphologically similar fungal isolates were sub-cultured and purified using the single spore isolation method (Choi et al. 1999). The fungus produced colonies with abundant, loosely floccose, whitish-brown aerial mycelia and pale-orange pigmentation on PDA. Macroconidia had four 4 to six 6 septa, a foot-shaped basal cell, an elongated apical cell, and a size of 20.2 to 31.8 × 2.2 to 4.1 μm (n = 30). No microconidia were observed. Chlamydospores were produced from hyphae and were most often intercalary, in pairs or solitary, globose, and frequently formed chains (6.2? to 11.7 μm, n = 10). Based on these morphological characteristics, the fungus was identified as Fusarium equiseti (Leslie and Summerell 2006). A representative isolate was deposited in the Korean Agricultural Culture Collection (KACC48935). For molecular characterization, portions of the translation elongation factor 1-alpha (TEF-1α) and second largest subunit of RNA polymerase II (RPB2) genes were amplified from the representative isolate using the primers pair of TEF-1α (O'Donnell et al. 2000) and GQ505815 (Fusarium MLST database), and sequenced. Searched BLASTn of the RPB2 sequence (MT576587) to the Fusarium MLST database showed 99.94% similarity to the F. incarnatum-equiseti species complex (GQ505850) and 98.85 % identity to both F. equiseti (GQ505599) and F. equiseti (GQ505772). Further, the TEF-1α sequence (MT084815) showed 100% identity to F. equiseti (KT224215) and 99.85% identity to F. equiseti (GQ505599), respectively. Therefore, the fungus was identified as F. equiseti based on morphological and molecular identification. For pathogenicity testing, a conidial suspension (1 × 106 conidia/ml) was prepared by harvesting macroconidia from 2-week-old cultures on PDA. Fifteen 4-week-old cabbage seedlings (cv. 12-Aadrika) were inoculated by dipping roots into the conidial suspension for 30 min. The inoculated plants were transplanted into a 50-hole plastic tray containing sterilized soil and maintained in a growth chamber at 25°C, with a relative humidity of >80%, and a 12-h/12-h light/dark cycle. After 4 days, the first wilt symptoms were observed on inoculated seedlings, and the infected plants eventually died within 1 to 2 weeks after inoculation. No symptoms were observed in plants inoculated with sterilized distilled water. The fungus was re-isolated from symptomatic tissues of inoculated plants and its colony and spore morphology were identical to those of the original isolate, thus confirming Koch's postulates. Fusarium wilt caused by F. equiseti has been reported in various crops, such as cauliflower in China, cumin in India, and Vitis vinifera in Spain (Farr and Rossman 2020). To our knowledge, this is the first report of F. equiseti causing Fusarium wilt on cabbage in Korea. It This disease poses a threat to cabbage production in Korea, and effective disease management strategies need to be developed.
    
  

  


              
            "
