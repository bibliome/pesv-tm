"Insects | Free Full-Text | The First Record of Monochamus saltuarius (Coleoptera; Cerambycidae) as Vector of Bursaphelenchus xylophilus and Its New Potential Hosts in China"
"
Pine wilt disease was first discovered in Dongtang town, Liaoning Province, China, in 2017. However, no record of Monochamus alteratus existed in Fengcheng, where M. saltuarius is an indigenous insect, and no experimental evidence has thus far indicated that M. saltuarius can transport the Bursaphelenchus xylophilus in China. In this study, we investigated whether M. saltuarius is a vector of B. xylophilus in China. On the sixth day after eclosion, beetles began to transmit nematodes into the twigs. The transmission period of nematodes is known to be able to last for 48 days after beetle emergence. In laboratory experiments, M. saltuarius fed and transmitted B. xylophilus not only on pines but also on other non-Pinus conifers. The non-Pinus conifers preferred by M. saltuarius for feeding are Picea pungens, Picea asperata, and Abies fabri. The experimental results show that M. saltuarius functions as a vector of B. xylophilus in northeast China.

View Full-Text
"
