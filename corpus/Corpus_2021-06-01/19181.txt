"
  
    
    
    
    
      
  Root samples provide early and improved detection of Candidatus Liberibacter asiaticus in Citrus


    
  
"
"
              
                


  
    
      
      Huanglongbing (HLB), or Citrus Greening, is one of the most devastating diseases affecting agriculture today. Widespread throughout Citrus growing regions of the world, it has had severe economic consequences in all areas it has invaded. With no treatment available, management strategies focus on suppression and containment. Effective use of these costly control strategies relies on rapid and accurate identification of infected plants. Unfortunately, symptoms of the disease are slow to develop and indistinct from symptoms of other biotic/abiotic stressors. As a result, diagnosticians have focused on detecting the pathogen, Candidatus Liberibacter asiaticus, by DNA-based detection strategies utilizing leaf midribs for sampling. Recent work has shown that fibrous root decline occurs in HLB-affected trees before symptom development among leaves. Moreover, the pathogen, Ca. Liberibacter asiaticus, has been shown to be more evenly distributed within roots than within the canopy. Motivated by these observations, a longitudinal study of young asymptomatic trees was established to observe the spread of disease through time and test the relative effectiveness of leaf- and root-based detection strategies. Detection of the pathogen occurred earlier, more consistently, and more often in root samples than in leaf samples. Moreover, little influence of geography or host variety was found on the probability of detection.
    
  

  


              
            "
