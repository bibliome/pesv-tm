"
  
    
    
    
    
      
  High genomic diversity of novel phages infecting the plant pathogen Ralstonia solanacearum, isolated in Mauritius and Reunion islands


    
  
"
"
              
                


  
    
      
      Bacterial wilt caused by the Ralstonia solanacearum species complex (RSSC) is among the most important plant diseases worldwide, severely affecting a high number of crops and ornamental plants in tropical regions. Only a limited number of phages infecting R. solanacearum have been isolated over the years, despite the importance of this bacterium and the associated plant disease. The antibacterial effect or morphological traits of these R. solanacearum viruses have been well studied, but not their genomic features, which need deeper consideration. This study reports the full genome of 23 new phages infecting RSSC isolated from agricultural samples collected in Mauritius and Reunion islands, particularly affected by this plant bacterial pathogen and considered biodiversity hotspots in the Southwest Indian Ocean. The complete genomic information and phylogenetic classification is provided, revealing high genetic diversity between them and weak similarities with previous related phages. The results support our proposal of 13 new species and seven new genera of R. solanacearum phages. Our findings highlight the wide prevalence of phages of RSSC in infected agricultural settings and the underlying genetic diversity. Discoveries of this kind lead more insight into the diversity of phages in general and to optimizing their use as biocontrol agents of bacterial diseases of plants in agriculture.
    
  

  


              
            "
