"
  
    
    
    
    
      
  Nematotoxicity of a Cyt-like protein toxin from Conidiobolus obscurus (Entomophthoromycotina) on the pine wood nematode Bursaphelenchus xylophilus


    
  
"
"
              
                


  
    
      
        
          Background:
        
      
      The pine wood nematode Bursaphelenchus xylophilus is a destructive pest on Pinus trees and lacks effective control measures. The present study identified a novel nematotoxic cytolytic (Cyt)-like protein originating from the entomopathogenic fungus Conidiobolus obscurus.
    
  

  


              
                


  
    
      
        
          Results:
        
      
      The protein was successfully purified using heterologous expression in Escherichia coli and affinity chromatography. N-hydroxysuccinimide-rhodamine-labeled Cyt-like protein was used to establish the route of toxin uptake, and revealed that the toxin can enter the nematode via the stylet. In bioassays, the purified protein had high nematicide activity against B. xylophilus, with a median lethal concentration at 24 h of 15.8 and 29.4 μg mL-1 for juveniles and adults, respectively. Compared with the deionized water control, fecundity, thrashing, and egg hatching were significantly reduced by 97%, 98%, and 83%, respectively, with 40 μg mL-1 Cyt-like protein at 24-36 h. Staining with Oil-Red-O showed a decrease in large lipid droplet formation in the protein-treated adult nematodes.
    
  

  


              
                


  
    
      
        
          Conclusion:
        
      
      The Cyt-like protein toxin possesses high nematicide activity against B. xylophilus with effects on nematode vitality and fecundity. The potential exists to use the Cyt-like protein for the control of B. xylophilus.
    
  

  


              
            "
