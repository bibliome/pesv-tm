"
  
    
    
    
    
      
  First Report of Wheat Common Bunt Caused by Tilletia laevis in Henan Province, China


    
  
"
"
              
                


  
    
      
      Wheat common bunt is a serious disease that may lead to yield losses of 75-80% in many wheat regions of the world (Mathre 1996). The disease may reduce yield and flour quality by producing trimethylamine, a compound that smells like rotting fish (Castlebury et al. 2005; Hoffmann 1981; Mathre 1996). Two closely related basidiomycete species, Tilletia caries (DC.) Tul. & C. Tul. [syn. T. tritici (Bjerk.) Wint.] and T. laevis J. G. Kühn [syn. T. foetida (Wallr.) Liro], cause wheat common bunt. Teliospore morphology is used to differentiate the two species. Teliospores of T. caries have reticulates on the surface while teliospores of T. laevis have a smooth surface (Pieczul et al. 2018). T. laevis was reported in Liaoning, Shaanxi, Shandong, Beijing, Hebei, Shanxi, Jilin, Heilongjiang, Jiangsu, Gansu, Xinjiang, Sichuan, Yunnan, Inner Mongolia, and Tibet (Guo 2011; Wang 1963), but not in Henan, the biggest wheat production province in China, before the present study. In July 2019, we found wheat common bunt in three fields grown with cultivar Zhengmai 618 in Yugong Mountain, Henan province. The diseased wheat heads had bunt balls filled with black powder with fishy smell. The disease incidences in these fields were 20-50%, but no common bunt was found in other nearby fields. About 200 diseased heads were sampled from the three fields. Teliospores from each head were observed under a microscope, and they all had smooth surface. Observations using a scanning electron microscope also showed smooth-surfaced teliospores. Teliospores were measured 13.5 to 18.5 μm in diameter. After surface sterilization of diseased heads using 0.25% NaClO for 5 min, teliospore suspension (1×106/ml) was made using sterilized distilled water and spread on water agar (200 μl per plate), and the plates were kept at 15°C with 24 h light (Goates and Hoffman 1987). On the 6th days, teliospores were germinated. Based on the disease symptoms, teliospore morphology, and germination, the bunt fungus was identified as T. laevis. To fulfill Koch's postulates, 1 ml of germinating teliospore suspension at the concentration of 106 spores/ml was injected into the heads of susceptible wheat cultivar (Dongxuan 3) at the boot stage with a syringe, and the plants injected with sterile ddH2O were used as control. The inoculated plants were grown in a growth chamber at 17°C with 50% humidity and 24 h light (300 μmol/m2/s). After one month at the ripening stage, the kernels of the inoculated plants were filled with black teliospores releasing fishy smell, and the control plants did not have bunt heads. Under a scanning electron microscope, teliospores from the inoculated heads had smooth surface and were measured 13.5 to 18.5 μm in diameter, similar to the teliospores of bunt heads from the fields. The fungus was also confirmed through molecular characterization using sequence characterized amplification region (SCAR) markers specific for T. laevis, and the expected 660 bp (Yao et al., 2019) and 286 bp (Zhang et al. 2012) bands were obtained separately from the teliospore samples from both the fields and growth chamber. The collection named as CGMCC 3.20112 was deposited in China General Microbiological Culture Collection Center. To the best of our knowledge, this is the first report of T. laevis causing wheat common bunt in Henan Province of China. Because the pathogen is seedborne and soilborne, the disease may become a high risk to wheat production in Henan and other provinces of China.
    
  

  


              
            "
