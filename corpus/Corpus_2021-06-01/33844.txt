"
  
    
    
    
    
      
  Huanglongbing and citrus variegated chlorosis integrated management based on favorable periods for vector population increase and symptom expression


    
  
"
"
              
                


  
    
      
      Huanglongbing (HLB, associated with 'Candidatus Liberibacter asiaticus' and transmitted by the Asian citrus psyllid Diaphorina citri) and citrus variegated chlorosis (CVC, caused by Xylella fastidiosa subsp. pauca and transmitted by sharpshooter species) have been managed by vector control and removal of symptomatic trees. Although vectors and new symptomatic trees can be detected year-round, peaks of vector populations are higher in spring and summer and the most symptomatic trees are found in autumn and winter. This work aimed to compare the management of both diseases during these favorable periods. The experiment was conducted over five years in a commercial orchard and had a 3 by 2 factorial design. The factor \"vector control\" had 3 levels: monthly vector control year-round (VCYR); monthly vector control in spring and summer (VCSS); and vector control when a threshold level of 10% occupancy was detected (VCOT). The factor \"inoculum removal\" had 2 levels: monthly eradication year-round (TEYR); and monthly eradication in autumn and winter (TEAW). Host flush, both HLB and CVC vector populations, and the number of symptomatic citrus plants were visually assessed. The level of vectors over the seasons, as measured using the average area under the curve (AUC), was similar for all treatments with the exception of psyllid abundance, which was around 4.5 times higher for VCSS than other treatments. For both diseases, no difference in the average AUC of disease progress and disease final incidence was observed. VCOT or adjusted VCSS associated to TEAW could be integrated for sustainable citrus production.
    
  

  


              
            "
