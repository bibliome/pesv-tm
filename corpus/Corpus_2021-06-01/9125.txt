"
  
    
    
    
    
      
  Development and evaluation of a loop-mediated isothermal amplification (LAMP) assay for the detection of Tomato brown rugose fruit virus (ToBRFV)


    
  
"
"
              
                


  
    
      
      Tomato brown rugose fruit virus (ToBRFV) is a member of Tobamovirus infecting tomato and pepper. Within North America, both the United States and Mexico consider ToBRFV to be a regulated pest. In Canada, the presence of ToBRFV has been reported, but an efficient diagnostic system has not yet been established. Here, we describe the development and assessment of a loop-mediated isothermal amplification (LAMP)-based assay to detect ToBRFV. The LAMP test was efficient and robust, and results could be obtained within 35 min with an available RNA sample. Amplification was possible when either water bath or oven were used to maintain the temperature at isothermal conditions (65°C), and results could be read by visual observation of colour change. Detection limit of the LAMP was eight target RNA molecules. Under the experimental conditions tested, LAMP was as sensitive as qPCR and 100 times more sensitive than the currently used RT-PCR. We recommend this sensitive, efficient LAMP protocol to be used for routine lab testing of ToBRFV.
    
  

  


              
            "
