"
  
    
    
    
    
      
  Can the Addition of Odor and Visual Targets Enhance Attraction of the Asian Citrus Psyllid (Hemiptera: Liviidae) to Sticky Traps?


    
  
"
"
              
                


  
    
      
      Asian citrus psyllid, Diaphorina citri Kuwayama, negatively impacts the citrus industry as it transmits Candidatus Liberibacter spp., the causal agent of citrus greening. Monitoring D. citri population levels is critical for management of vectors and citrus greening and is usually through use of yellow sticky traps. In our study, use of odors, odor blends, and visual targets were evaluated to determine whether attraction to yellow sticky traps could be improved. Methyl salicylate consistently increased D. citri attraction to decoy yellow but not to other yellow or yellow/green traps. Addition of a visual target did not enhance attraction to sticky traps. While several chemical blends were evaluated, they did not increase psyllid attraction to decoy yellow traps. The increased attraction to traps with methyl salicylate is promising and may contribute to trapping efficiency under field conditions.
    
  

  


              
            "
