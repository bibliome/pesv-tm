"
  
    
    
    
    
      
  A New Bacteria-Free Strategy Induced by MaGal2 Facilitates Pinewood Nematode Escape Immune Response From Its Vector Beetle


    
  


            
              


  
    

    
      Symbiotic microbes play a crucial role in regulating parasite-host interactions; however, the role of bacterial associates in parasite-host interactions requires elucidation. In this study, we showed that, instead of introducing numerous symbiotic bacteria, dispersal of fourth-stage juvenile (JIV ) pinewood nematodes (PWNs), Bursaphelenchus xylophilus, only introduced few bacteria to its vector beetle, Monochamus alternatus (Ma). JIV showed weak binding ability to five dominant bacteria species isolated from the beetles' pupal chamber. This was especially the case for binding to the opportunistic pathogenic species Serratia marcescens; the nematodes' bacteria binding ability at this critical stage when it infiltrates Ma for dispersal was much weaker compared with Caenorhabditis elegans, Diplogasteroides asiaticus, and propagative-stage PWN. The associated bacterium S. marcescens, which was isolated from the beetles' pupal chambers, was unfavorable to Ma, because it caused a higher mortality rate upon injection into tracheae. In addition, S. marcescens in the tracheae caused more immune effector disorders compared with PWN alone. Ma_Galectin2 (MaGal2), a pattern-recognition receptor, was up-regulated following PWN loading. Recombinant MaGal2 protein formed aggregates with five dominant associated bacteria in vitro. Moreover, MaGal2 knockdown beetles had up-regulated prophenoloxidase gene expression, increased phenoloxidase activity, and decreased PWN loading. Our study revealed a previously unknown strategy for immune evasion of this plant pathogen inside its vector, and provides novel insights into the role of bacteria in parasite-host interactions. This article is protected by copyright. All rights reserved.
    

    
  


            
          "
