"
  
    
    
    
    
      
  Predicting Virulence of Fusarium Oxysporum F. Sp. Cubense Based on the Production of Mycotoxin Using a Linear Regression Model


    
  


            
              


  
    

    
      Fusarium wilt caused by Fusarium oxysporum f.sp. cubense (Foc) is one of the most destructive diseases for banana. For their risk assessment and hazard characterization, it is vital to quickly determine the virulence of Foc isolates. However, this usually takes weeks or months using banana plant assays, which demands a better approach to speed up the process with reliable results. Foc produces various mycotoxins, such as fusaric acid (FSA), beauvericin (BEA), and enniatins (ENs) to facilitate their infection. In this study, we developed a linear regression model to predict Foc virulence using the production levels of the three mycotoxins. We collected data of 40 Foc isolates from 20 vegetative compatibility groups (VCGs), including their mycotoxin profiles (LC-MS) and their plant disease index (PDI) values on Pisang Awak plantlets in greenhouse. A linear regression model was trained from the collected data using FSA, BEA and ENs as predictor variables and PDI values as the response variable. Linearity test statistics showed this model meets all linearity assumptions. We used all data to predict PDI with high fitness of the model (coefficient of determination (R2 = 0.906) and adjust coefficient (R2adj = 0.898)) indicating a strong predictive power of the model. In summary, we developed a linear regression model useful for the prediction of Foc virulence on banana plants from the quantification of mycotoxins in Foc strains, which will facilitate quick determination of virulence in newly isolated Foc emerging Fusarium wilt of banana epidemics threatening banana plantations worldwide.
    

    
  


            
          "
