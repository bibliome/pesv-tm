"
  
    
    
    
    
      
  Soil and Leaf Ionome Heterogeneity in Xylella fastidiosa Subsp. Pauca-Infected, Non-Infected and Treated Olive Groves in Apulia, Italy


    
  


            
              


  
    

    
      Xylella fastidiosa subsp. pauca is responsible for the \"olive quick decline syndrome\" (OQDS) in Salento (Apulia). The main epidemiological aspects of the syndrome are related to the pathogen spread and survival in the area, and to the biology of the insect vector. The assessment of the macro and microelements content (i.e., ionome) in soil and leaves could provide basic and useful information. Indeed, knowledge of host ionomic composition and the possibility of its modification could represent a potential tool for the management of diseases caused by X. fastidiosa. Therefore, soil and leaf ionomes of naturally infected, not infected, and zinc-copper-citric acid biocomplex treated trees of different areas of Apulia and the bordering Basilicata regions were compared. We observed that soil and leaf ionomic composition of olive farms growing in the pathogen-free areas north of the Salento Barletta-Andria-Trani BAT (Apulia) and Potenza PZ (Basilicata, Apulia bordering region) provinces is significantly different from that shown by the infected olive groves of the Salento areas (LE, BR, TA provinces). In particular, a higher content of zinc and copper both in soil and leaves was found in the studied northern areas in comparison to the southern areas. This finding could partly explain the absence of OQDS in those areas. In the infected Salento areas, the leaf ionomic profile resulted as being markedly different for the biocomplex treated compared to the untreated trees. A higher zinc content in leaves characterized treated with respect to untreated trees. On the other hand, among the not-infected trees, Xylella-resistant Leccino showed higher manganese content when compared with the higher pathogen sensitive Ogliarola salentina and Cellina di Nardò. According to these results, soil and olive leaf ionome could provide basic information for the epidemiologic study and possible control of X. f. subsp. pauca in Apulia.
    

    
  


            
          "
