"
  
    
    
    
    
      
  An Emerging Strawberry Fungal Disease Associated With Root Rot, Crown Rot and Leaf Spot Caused by Neopestalotiopsis rosae in Mexico


    
  


            
              


  
    

    
      In the 2017 strawberry season, several transplant losses reaching 50% were observed in Zamora, Michoacán Valley, Mexico, due to a new fungal disease associated with root rot, crown rot, and leaf spot. In this year the disease appeared consistently and increased in the following seasons, becoming a concern among strawberry growers. Thus, the aim of this research was to determine the etiology of the disease and to determine the in vitro effect of fungicides on mycelial growth of the pathogen. Fungal isolates were obtained from symptomatic strawberry plants of the cultivars 'Albion' and 'Festival' and were processed to obtain monoconidial isolates. Detailed morphological analysis was conducted. Concatenated phylogenetic reconstruction was conducted by amplifying and sequencing the translation elongation factor 1 α, β-tubulin partial gene, and the internal transcribed spacer region of rDNA. Pathogenicity tests involving inoculation of leaves and crowns reproduced the same symptoms as those observed in the field, fulfilling Koch's postulates. Morphology and phylogenetic reconstruction indicated that the causal agent of the described symptoms was Neopestalotiopsis rosae, marking the first report anywhere in the world of this species infecting strawberry. N. rosae was sensitive to cyprodinil + fludioxonil, captan, iprodione, difenoconazole, and prochloraz.
    

    
  


            
          "
