"
  
    
    
    
    
      
  Characterization of Bacterial Communities Associated With the Pinewood Nematode Insect Vector Monochamus Alternatus Hope and the Host Tree Pinus Massoniana


    
  


            
              


  
    
      
        Background:
      
    

    
      Monochamus alternatus Hope is one of the insect vectors of pinewood nematode (Bursaphelenchus xylophilus), which causes the destructive pine wilt disease. The microorganisms within the ecosystem, comprising plants, their environment, and insect vectors, form complex networks. This study presents a systematic analysis of the bacterial microbiota in the M. alternatus midgut and its habitat niche.
    

    
  


            
              


  
    
      
        Methods:
      
    

    
      Total DNA was extracted from 20 types of samples (with three replicates each) from M. alternatus and various tissues of healthy and infected P. massoniana (pines). 16S rDNA amplicon sequencing was conducted to determine the composition and diversity of the bacterial microbiota in each sample. Moreover, the relative abundances of bacteria in the midgut of M. alternatus larvae were verified by counting the colony-forming units.
    

    
  


            
              


  
    
      
        Results:
      
    

    
      Pinewood nematode infection increased the microbial diversity in pines. Bradyrhizobium, Burkholderia, Dyella, Mycobacterium, and Mucilaginibacter were the dominant bacterial genera in the soil and infected pines. These results indicate that the bacterial community in infected pines may be associated with the soil microbiota. Interestingly, the abundance of the genus Gryllotalpicola was highest in the bark of infected pines. The genus Cellulomonas was not found in the midgut of M. alternatus, but it peaked in the phloem of infected pines, followed by the phloem of heathy pines. Moreover, the genus Serratia was not only present in the habitat niche, but it was also enriched in the M. alternatus midgut. The colony-forming unit assays showed that the relative abundance of Serratia sp. peaked in the midgut of instar II larvae (81%).
    

    
  


            
              


  
    
      
        Conclusions:
      
    

    
      Overall, the results indicate that the bacterial microbiota in the soil and in infected pines are correlated. The Gryllotalpicola sp. and Cellulomonas sp. are potential microbial markers of pine wilt disease. Additionally, Serratia sp. could be an ideal agent for expressing insecticidal protein in the insect midgut by genetic engineering, which represents a new use of microbes to control M. alternatus.
    

    
  


            
          "
