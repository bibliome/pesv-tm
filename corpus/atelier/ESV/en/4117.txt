"HLB detected in Kenya: South African citrus growers advised to monitor for arrival of disease vector

        Huanglongbing (HLB), or Asian greening disease, has been detected in Kenya four years after the disease vector, the Asian citrus psyllid (ACP), was detected in the country. The three detections of HLB in the east of Kenya are less than 100 km apart: one in a backyard and the other two in research orchards, one with a citrus nursery, according to Citrus Research International, working in close collaboration with International Centre of Insect Physiology and Ecology (ICIPE) in Kenya, as well as Tanzanian scientists.
The limited distribution of the detection in Kenya holds out hope that eradication is still possible, but it would depend on the traceability system of the citrus nursery where the disease was found as well as a proper delimitation survey that needs to be undertaken without delay by Kenyan authorities.
The vector has not been detected in Uganda.
Studies need to determine what percentage of the population of the Asian citrus psyllid in this region is in fact infected with the HLB bacteria Liberibacter asiaticus (Las) by officials from the Kenya Plant Health Inspectorate Services (KEPHIS).
“As a follow-on from potential containment or eradication in Kenya, ongoing actions should continue to delimit the southern distribution range of HLB and ACP [Asian citrus psyllid]. That includes seeking verification of previously reported finds of ACP in southern Tanzania, as well as extending surveys northwards in Mozambique. Accordingly, CRI will engage collaborators in Tanzania to collect ACP from southern Tanzania and send them to CRI for preliminary identification using morphological features,” CRI states in its May newsletter.
“The recent detection of HLB in Kenya brings it considerably closer to citrus production in southern Africa. In southern African countries, early warning surveillance has been initiated in eSwatini, Zimbabwe, Namibia, Angola, Zambia, and Mozambique, with no Las or ACP detection in these countries so far.”
Detection sites (circled) of HLB in Ethiopia and Kenya (figure from Ajene et al. 2020)
In South Africa, coordinated early detection surveys for ACP using sticky traps have been initiated in commercial orchards in Letsitele and the Onderberg regions of Limpopo. CRI has communicated to growers that they should start doing the same, especially growers close to neighbouring countries.
Twelve inspectors at the South African Department of Agriculture, Land Reform and Rural Development have been trained in detection of the Asian citrus psyllid.
An HLB and ACP Action Plan and HLB Safe Nursery System has been developed by CRI and officially adopted by the Department of Agriculture, Land Reform and Rural Development, to guide national efforts to protect Southern Africa’s citrus industry from HLB.
Citrus nurseries are encouraged to quickly move over, if they haven’t yet done so, to insect-secure structures as per stipulations of the HLB and ACP Action Plan and the HLB Safe Nursery System. 
An insect-secure citrus nursery
For more information:
Citrus Research International
Tel: +27 13 759 8000
www.citrusres.com
    "
