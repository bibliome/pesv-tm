"
  
    
    
    
    
      
  BxCDP1 From the Pine Wood Nematode Bursaphelenchus Xylophilus Is Recognized as a Novel Molecular Pattern


    
  


            
              


  
    

    
      The migratory plant-parasitic nematode Bursaphelenchus xylophilus is the causal agent of pine wilt disease, which causes serious damage to pine forests in China. Plant immunity plays an important role in plant resistance to multiple pathogens. Activation of the plant immune system is generally determined by immune receptors, including plant pattern recognition receptors, which mediate pattern recognition. However, little is known about molecular pattern recognition in the interaction between pines and B. xylophilus. Based on the B. xylophilus transcriptome at the early stages of infection and Agrobacterium tumefaciens-mediated transient expression and infiltration of recombinant proteins produced by Pichia pastoris in many plant species, a novel molecular pattern (BxCDP1) was characterized in B. xylophilus. We found that BxCDP1 was highly up-regulated at the early infection stages of B. xylophilus, and was similar to a protein in Pararhizobium haloflavum. BxCDP1 triggered cell death in Nicotiana benthamiana when secreted into the apoplast, and this effect was dependent on brassinosteroid-insensitive 1-associated kinase 1, but independent of suppressor of BIR1-1. BxCDP1 also exhibited cell death-inducing activity in pine, Arabidopsis, tomato, pepper, and lettuce. BxCDP1 triggered reactive oxygen species production and the expression of PAMP-triggered immunity marker genes (NbAcre31, NbPTI5, and NbCyp71D20) in N. benthamiana. It also induced the expression of pathogenesis-related genes (PtPR-3, PtPR-4, and PtPR-5) in Pinus thunbergii. These results suggest that as a new B. xylophilus molecular pattern, BxCDP1 can not only be recognized by many plant species, but also triggers innate immunity in N. benthamiana and defence responses of P. thunbergii.
    

    
  


            
          "
