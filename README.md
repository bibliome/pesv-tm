>> This is work in progress, [contact us](mailto:mouhamadou%DOT%ba%AT%inrae%DOT%fr) if you have issues and questions.

# About

This project is designed to extract entities (i.e., `taxa`, `phenotypes`, `habitats`, `disease names`, `hosts`, `pathogen`, `vector`, `dates` and `geographic names`) from textual data for the purpose of scientific watch.

The project contains a workflow based on Framework [AlvisNLP](https://github.com/Bibliome/alvisnlp) and uses the Ontobiotope Ontology and NCBI taxonomy.

## Requirements

* GNU Bash 4.4.X
* A git client
* [singularity](https://sylabs.io/) 3.4.X ([how to install singularity ?](https://sylabs.io/guides/3.4/user-guide/quick_start.html#quick-installation-steps))
 
In order to run the workflow you will need a host with at least 16Gb RAM.

## Usage

The current version is compatible with [`AlvisNLP version 0.8.2`](https://github.com/Bibliome/alvisnlp/).
Installation of AlvisNLP is not required, it is provided in the singularity images. 

Run in command line the following steps to test the workflow,
a test corpus is provided here `corpus/pesv/Xylella-test/txt/`, `16Go` RAM is required to process the test corpus).


### Clone the project

1. clone the project.

```sh
git clone https://forgemia.inra.fr/bibliome/pesv-tm.git
cd pesv-tm
```

2. pull the singularity image of AlvisNLP (the image size is ~4Go, pulling may take a while)

```sh
cd softwares
singularity pull library://migale/default/alvisnlp:0.8.2
```

<!--
singularity pull alvisnlp-0.7.1.sif oras://registry.forgemia.inra.fr/bibliome/pesv-tm/alvisnlp:0.7.1
-->

### Generate the super-taxonomy

The submodule [Extended microorganisms taxonomy](https://forgemia.inra.fr/omnicrobe/extended-microorganisms-taxonomy) is available into Folder `softwares`. Set the default params and generate the taxo as described in the submodule [README](softwares/extended-microorganisms-taxonomy/README.md)

<!--
2. pull the singularity image of AlvisNLP (the image size is ~4Go, pulling may take a while)

> If you encounter permission issues to pull the singularity image, or if the server asks for authentification, then please contact the [maintainer](mailto:mouhamadou%DOT%ba%AT%inrae%DOT%fr).

```sh
$ cd pesv-tm

$ singularity pull alvisnlp-0.7.1.sif oras:registry.forgemia.inra.fr/bibliome/pesv-tm/alvisnlp:0.7.1
```
-->

### Run the workflow. 

You will need a corpus to process.
The image contains a test corpus in `corpus/pesv/Xylella-test/txt/3IO4RO27QI.txt`.

```sh
$ softwares/alvisnlp-0.8.1.sif -J-Xmx16G -verbose -cleanTmp \
    -alias input corpus/pesv/Xylella-test/txt/3IO4RO27QI.txt \
    -outputDir corpus/pesv/Xylella-test/ \
    -entity ontobiotope resources/BioNLP-OST+EnovFood \
    -feat inhibit-syntax inhibit-syntax \
    plans/PESV_workflow.plan
```

AlvisNLP will process the corpus.
In this example, the output is stored in `corpus/pesv/Xylella-test/`.


### Run and check the results

```sh
$ firefox corpus/Xylella/visualisation_html/index.html
```

You can run AlvisNLP with the browser mode:
```sh
$ firefox http://localhost:8878
$ softwares/alvisnlp-0.8.1.sif -J-Xmx16G -verbose -cleanTmp \
    -browser \
    -alias input corpus/pesv/Xylella-test/txt/3IO4RO27QI.txt \
    -outputDir corpus/pesv/Xylella-test/ \
    -entity ontobiotope resources/BioNLP-OST+EnovFood \
    -feat inhibit-syntax inhibit-syntax \
    plans/PESV_workflow.plan
```

Then refresh the page in the browser.

## Maintainer

[Mouhamadou Ba](mailto:mouhamadou%DOT%ba%AT%inrae%DOT%fr)
