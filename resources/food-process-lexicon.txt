processed	processed	processed	OBT:000828
raw	raw	raw	OBT:000834
raw	raw	raw	OBT:000834
uncooked	uncooked	uncooked	OBT:000834
unprocessed	unprocessed	unprocessed	OBT:000834
untreated	untreated	untreated	OBT:000834
defrosted	defrosted	defrosted	OBT:001066
thawed	thawed	thawed	OBT:001066
ground	ground	ground	OBT:001138
home-made	home - made	home - made	OBT:001145
home-prepared	home - prepared	home - prepared	OBT:001145
mashed	mashed	mashed	OBT:001199
milled	milled	milled	OBT:001209
preserved	preserved	preserved	OBT:001266
pressed	pressed	pressed	OBT:001267
canned	canned	canned	OBT:001473
concentrated	concentrated	concentrated	OBT:001497
cooked	cooked	cooked	OBT:001499
cooled	cooled	cooled	OBT:001500
dried	dried	dried	OBT:001526
dehydrated	dehydrated	dehydrated	OBT:001526
dry	dry	dry	OBT:001526
fermented	fermented	fermented	OBT:001546
frozen	frozen	frozen	OBT:001564
heat-preserved	heat - preserved	heat - preserved	OBT:001592
ionised	ionised	ionised	OBT:001603
ionized	ionized	ionized	OBT:001603
irradiated	irradiated	irradiated	OBT:001603
jarred	jarred	jarred	OBT:001604
marinated	marinated	marinated	OBT:001629
packed	packed	packed	OBT:001687
powdered	powdered	powdered	OBT:001728
pressure treated	pressure treated	pressure treated	OBT:001729
pressure processed	pressure processed	pressure processed	OBT:001729
pressure-treated	pressure - treated	pressure - treated	OBT:001729
pressuretreated	pressuretreated	pressuretreated	OBT:001729
pressurized	pressurized	pressurized	OBT:001729
salt-preserved	salt - preserved	salt - preserved	OBT:001755
smoked	smoked	smoked	OBT:001774
sugar-preserved	sugar - preserved	sugar - preserved	OBT:001804
UHT	UHT	UHT	OBT:001855
baked	baked	baked	OBT:001889
blanched	blanched	blanched	OBT:001905
boiled	boiled	boiled	OBT:001908
candied	candied	candied	OBT:001926
cured	cured	cured	OBT:001984
deteriorated canned	deteriorated canned	deteriorated canned	OBT:001995
fried	fried	fried	OBT:002047
griddled	griddled	griddled	OBT:002068
griddle cooked	griddle cooked	griddle cooked	OBT:002068
griddle-cooked	griddle - cooked	griddle - cooked	OBT:002068
grilled	grilled	grilled	OBT:002069
barbecued	barbecued	barbecued	OBT:002069
broiled	broiled	broiled	OBT:002069
high pressure treated	high pressure treated	high pressure treated	OBT:002079
high-pressure-treated	high - pressure - treated	high - pressure - treated	OBT:002079
modified-atmosphere-packed	modified - atmosphere - packed	modified - atmosphere - packed	OBT:002140
pasteurized	pasteurized	pasteurized	OBT:002173
pickled	pickled	pickled	OBT:002184
poached	poached	poached	OBT:002193
reheated	reheated	reheated	OBT:002221
roasted	roasted	roasted	OBT:002228
salted	salted	salted	OBT:002238
scalded	scalded	scalded	OBT:002240
simmered	simmered	simmered	OBT:002249
steamed	steamed	steamed	OBT:002267
sterilized	sterilized	sterilized	OBT:002268
sterilised	sterilised	sterilised	OBT:002268
stewed	stewed	stewed	OBT:002269
vacuum-packed	vacuum - packed	vacuum - packed	OBT:002300
fermented plant-based	fermented plant - based	fermented plant - based	OBT:003047
